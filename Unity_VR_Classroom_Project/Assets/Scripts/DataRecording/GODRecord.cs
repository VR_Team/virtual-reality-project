﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class GODRecord : MonoBehaviour {
	
	public string fileName;
	private	string[] data = new string[MACROS.Constants.DATAMESSAGE_NUMTYPES];
	private string pathName;
	private bool firstwriteFlag = false;

	private const string lookatsuffix = ".lookingat.csv";
	private const string mathtasksuffix = ".mathtask.csv";
	private const string animeventsuffix = ".event.csv";
	private const string headtracksuffix = ".headtrack.csv";
	private const string pausesuffix = ".pause.csv";

	
	// Use this for initialization
	void Start () {
		pathName = Application.dataPath + "/DataOutput";
		if(!Directory.Exists(pathName))
		{   
			Directory.CreateDirectory(pathName);
			
		}
		for (int i = 0; i < MACROS.Constants.DATAMESSAGE_NUMTYPES; i++) {
			data[i] = "";	
		}
		pathName += "/";
	}
	
	// Update is called once per frame
	void Update () {
		writeMsg ();
	}

	public void storeMsg(AssemblyCSharp.DataMessage msgIn){
		switch (msgIn.messageType) {
		case(MACROS.Constants.DATAMESSAGE_LOOKINGAT):
			data[MACROS.Constants.DATAMESSAGE_LOOKINGAT] += (msgIn.objectName + "," + msgIn.data + "\n");
			break;
		case(MACROS.Constants.DATAMESSAGE_MATHTASK):
			data[MACROS.Constants.DATAMESSAGE_MATHTASK] += (msgIn.objectName + "," + msgIn.data + "\n");
			break;
		case(MACROS.Constants.DATAMESSAGE_STUDENTANIMATIONEVENT):
			data[MACROS.Constants.DATAMESSAGE_STUDENTANIMATIONEVENT] += (msgIn.objectName + "," + msgIn.data + "\n");
			break;
		case(MACROS.Constants.DATAMESSAGE_HEADTRACKING):
			data[MACROS.Constants.DATAMESSAGE_HEADTRACKING] += (msgIn.objectName + "," + msgIn.data + "\n");
			break;
		case(MACROS.Constants.DATAMESSAGE_PAUSE):
			data[MACROS.Constants.DATAMESSAGE_PAUSE] += (msgIn.objectName + "," + msgIn.data + "\n");
			break;
		}

	}

	private void writeMsg(){
		if (!firstwriteFlag) {
			deleteOldFile();
			firstwriteFlag = true;
		}

		File.AppendAllText (pathName + lookatsuffix, data[MACROS.Constants.DATAMESSAGE_LOOKINGAT].ToString());
		File.AppendAllText (pathName + mathtasksuffix, data[MACROS.Constants.DATAMESSAGE_MATHTASK].ToString());
		File.AppendAllText (pathName + animeventsuffix, data[MACROS.Constants.DATAMESSAGE_STUDENTANIMATIONEVENT].ToString());
		File.AppendAllText (pathName + headtracksuffix, data[MACROS.Constants.DATAMESSAGE_HEADTRACKING].ToString());
		File.AppendAllText (pathName + pausesuffix, data[MACROS.Constants.DATAMESSAGE_PAUSE].ToString());


		
		for (int i = 0; i < MACROS.Constants.DATAMESSAGE_NUMTYPES; i++) {
			data[i] = "";	
		}
	}
	
	public void fileNamebase(string name){
		pathName += name;
	}

	private void deleteOldFile(){
		if (File.Exists(pathName + lookatsuffix)){
			File.Delete(pathName + lookatsuffix);
		}
		if (File.Exists(pathName + mathtasksuffix)){
			File.Delete(pathName + mathtasksuffix);
		}
		if (File.Exists(pathName + animeventsuffix)){
			File.Delete(pathName + animeventsuffix);
		}
		if (File.Exists(pathName + headtracksuffix)){
			File.Delete(pathName + headtracksuffix);
		}
	}
}
