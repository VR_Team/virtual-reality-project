﻿using UnityEngine;
using System.Collections;

public class RecordLookedAt : MonoBehaviour {

	private bool lookingAt = false;
	private bool lastLookingAt = false;
	private GODRecord recordScriptHandle;
	private GODPause pauseScriptHandle;
	private AssemblyCSharp.DataMessage msg;

	// Use this for initialization
	void Start () {
		recordScriptHandle = GameObject.Find ("God").GetComponent<GODRecord> ();
		pauseScriptHandle = GameObject.Find ("God").GetComponent<GODPause> ();
	}
	
	// Update is called once per frame
	void Update () {
		print (lastLookingAt + " __ " + lookingAt);
		if (lastLookingAt != lookingAt) {
			msg = new AssemblyCSharp.DataMessage();
			msg.objectName = this.name;
			msg.messageType = MACROS.Constants.DATAMESSAGE_LOOKINGAT;
			msg.data = pauseScriptHandle.getTotalTime().ToString();
			if(lookingAt == true){
				msg.data += ",Start Looking At";
				lastLookingAt = true;
			}
			else{
				msg.data += ",Stop Looking At";
				lastLookingAt = false;
			}
			recordScriptHandle.storeMsg(msg);
		}
		lookingAt = false;
	}

	public void SetLookingAt(){
		print ("Looking At");
		lookingAt = true;
	}
}
