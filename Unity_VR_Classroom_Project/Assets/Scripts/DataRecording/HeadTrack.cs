﻿using UnityEngine;
using System.Collections;

public class HeadTrack : MonoBehaviour {

	public float minSecondsBetweenRecords = 0.0f;
	private float timeSinceLastRecord = 0.0f;

	private AssemblyCSharp.DataMessage msg;
	private GODPause pauseScriptHandle;
	private GODRecord recordScriptHandle;
	private bool recordPositionFlag;

	// Use this for initialization
	void Start () {
		recordPositionFlag = false;
		pauseScriptHandle = GameObject.Find ("God").GetComponent<GODPause> ();
		recordScriptHandle = GameObject.Find ("God").GetComponent<GODRecord> ();
	}

	private void RecordPosition(){
		Vector3 coordinates = this.transform.position;
		msg = new AssemblyCSharp.DataMessage();
		msg.objectName = this.gameObject.name;
		msg.data = coordinates.x + "," + coordinates.y + "," + coordinates.z;
		msg.messageType = MACROS.Constants.DATAMESSAGE_HEADTRACKING;
		recordScriptHandle.storeMsg (msg);
		recordPositionFlag = true;
	}

	public void Record() {
		Vector3 angle = this.transform.rotation.eulerAngles;
		msg = new AssemblyCSharp.DataMessage();
		msg.objectName = this.gameObject.name;
		msg.data = pauseScriptHandle.getTotalTime() + "," + angle.x + "," + angle.y + "," + angle.z;
		msg.messageType = MACROS.Constants.DATAMESSAGE_HEADTRACKING;
		recordScriptHandle.storeMsg (msg);
	}

	// Update is called once per frame
	void Update () {
		if(!recordPositionFlag){
			RecordPosition();
		}
		if (pauseScriptHandle.isRunning ()) {
			timeSinceLastRecord += minSecondsBetweenRecords;
			if(timeSinceLastRecord >= minSecondsBetweenRecords){
				Record();
			}
		}
	}
}
