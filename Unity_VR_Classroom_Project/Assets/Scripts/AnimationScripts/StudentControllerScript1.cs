﻿using UnityEngine;
using System.Collections;

public class StudentControllerScript1 : MonoBehaviour {

	public float animatorSpeed = 1.0f;
	public AudioClip sneeze;
	public float sneezePitch = 1.0f;
	public float sneezeVolume = 1.0f;
	public float rollsPerSecond = 25.0f;

	//State change probabilities
	public float sneezeProb = 3.0f;
	public float talkProb = 3.0f;

	public float tapFingerProb = 5.0f;
	public float scratchHeadProb = 5.0f;
	public float stretch0Prob = 10.0f;

	public float tapFingerLoopProb = 70.0f;

	public float unstretch1Prob = 50.0f;

	public float leanBack1Prob = 10.0f;
	public float stretch1Prob = 10.0f;

	public float stopLeanLoopProb = 10.0f;

	private Animator anim;
	private AnimatorStateInfo baseCurState;
	private AnimatorStateInfo reactCurState;
	private float diceRoll;
	private AudioSource audioSrc;
	private float timeCount;
	private int lastBaseState;
	private int lastReactionState;
	private GODRecord recordScriptHandle;
	private GODPause pauseScriptHandle;
	private AssemblyCSharp.DataMessage msg;

	static int idleState0 = Animator.StringToHash("Base Layer.IdleSit_0");
	static int idleState0_TapFingerR = Animator.StringToHash("Base Layer.IdleSit_0_TapRightFinger");
	static int idleState0_ScratchHeadR = Animator.StringToHash("Base Layer.IdleSit_0_RightHandScratchHead");
	static int idleState0_Stretch = Animator.StringToHash("Base Layer.IdleSit_0_Stretch");
	static int unstretch_to_idleState0 = Animator.StringToHash("Base Layer.Stretch_to_IdleSit_0");
	static int idleState1 = Animator.StringToHash("Base Layer.IdleSit_1");
	static int idleState1_Stretch = Animator.StringToHash("Base Layer.IdleSit_1_Stretch");
	static int idleState1_Lean_Loop = Animator.StringToHash("Base Layer.IdleSit_1_Lean_Back_Kick_Feet_Loop");
	static int unstretch_to_idleState1 = Animator.StringToHash("Base Layer.Stretch_to_IdleSit_1");
	static int leanBackStart = Animator.StringToHash("Base Layer.IdleSit_1_Lean_Back_Start");
	static int kickFeetStart = Animator.StringToHash("Base Layer.IdleSit_1_Lean_Back_Kick_Feet_Start");
	static int kickFeetStop = Animator.StringToHash ("Base Layer.IdleSit_1_Lean_Back_Kick_Feet_Stop");
	static int leanBackStop = Animator.StringToHash("Base Layer.IdleSit_1_Lean_Back_Stop");

	static int noReaction = Animator.StringToHash("Reactions.Calm");
	static int Sneeze = Animator.StringToHash("Reactions.Sneeze");
	static int Talk = Animator.StringToHash("Reactions.Talk");

	// Use this for initialization
	void Start () {
		recordScriptHandle = GameObject.Find ("God").GetComponent<GODRecord> ();
		pauseScriptHandle = GameObject.Find ("God").GetComponent<GODPause> ();
		anim = this.GetComponent<Animator> ();
		anim.SetLayerWeight (0, 1);
		anim.SetLayerWeight (1, 1);
		audioSrc = this.GetComponent<AudioSource> ();
		diceRoll = Random.Range (0,100);
		lastBaseState = anim.GetCurrentAnimatorStateInfo (0).nameHash;
		lastReactionState = anim.GetCurrentAnimatorStateInfo (1).nameHash;
	}

	void PlaySneeze() {
		audioSrc.pitch = sneezePitch;
		audioSrc.volume = sneezeVolume;

		audio.Play();
	}

	void stopReaction() {
		anim.SetBool("Ready",true);
		anim.SetInteger ("ReactionNumber", 0);
	}

	void chooseReaction() {
		diceRoll = Random.Range (0,100);
		if (diceRoll < sneezeProb){
			anim.SetInteger("ReactionNumber",1);
		} else if (diceRoll < sneezeProb + talkProb) {
			anim.SetInteger("ReactionNumber",2);
		} else {
			anim.SetInteger("ReactionNumber",0);
			anim.SetBool("Ready", true);
		}
	}

	string getStateString (int hash){
		if (idleState0 == hash) {
						return "Idle State 0";
				} else if (idleState0_TapFingerR == hash) {
						return "Idle 0 Tap Finger";
				} else if (idleState0_ScratchHeadR == hash) {
						return "Idle 0 Scratch Head";
				} else if (idleState0_Stretch == hash) {
						return "Idle 0 to Stretch";
				} else if (unstretch_to_idleState0 == hash) {
						return "Stretch to Idle 0";
				} else if (idleState1 == hash) {
						return "Idle State 1";
				} else if (idleState1_Stretch == hash) {
						return "Idle 1 to Stretch";
				} else if (idleState1_Lean_Loop == hash) {
						return "Kick Feet loop";
				} else if (unstretch_to_idleState1 == hash) {
						return "Stretch to Idle 1";
				} else if (leanBackStart == hash) {
						return "Idle 1 to Lean Back Start";
				} else if (kickFeetStart == hash) {
						return "Lean Back Start to Kick Feet Start";
				} else if (kickFeetStop == hash) {
						return "Kick Feet Loop to Kick Feet Stop";
				} else if (leanBackStop == hash) {
						return "Kick Feet Stop to Lean Back Stop";
				} else if (noReaction == hash) {
						return "No Reaction";
				} else if (Sneeze == hash) {
						return "Sneeze Reaction";
				} else if (Talk == hash) {
						return "Talking Reaction";
				} else {
						return "Unrecognized";
				}
	}

	private void SendStateMessage(int hash){
		msg = new AssemblyCSharp.DataMessage ();

		msg.objectName = this.gameObject.name;
		msg.data = pauseScriptHandle.getTotalTime ().ToString() + getStateString(hash);
		msg.messageType = MACROS.Constants.DATAMESSAGE_STUDENTANIMATIONEVENT;

		recordScriptHandle.storeMsg (msg);
	}

	private void Running(){
		if (rollsPerSecond <= 0) {
			return;
		}

		baseCurState = anim.GetCurrentAnimatorStateInfo (0);
		reactCurState = anim.GetCurrentAnimatorStateInfo (1);

		if (lastBaseState != baseCurState.nameHash) {
			SendStateMessage(baseCurState.nameHash);
		}

		if (lastReactionState != reactCurState.nameHash) {
			SendStateMessage(reactCurState.nameHash);
		}

		timeCount += Time.deltaTime;

		if (timeCount >= (1.0f / rollsPerSecond)) {
			diceRoll = Random.Range (0.0f, 100.0f);
			timeCount = 0;
		}

		if (baseCurState.nameHash == idleState0) {

			//print ("in idleState0");
			if (diceRoll < tapFingerProb) {
				anim.SetInteger ("EventNumber", 1);
				stopReaction ();
			} else if (diceRoll < tapFingerProb + scratchHeadProb) {
				anim.SetInteger ("EventNumber", 2);
				stopReaction ();
			} else if (diceRoll < stretch0Prob + tapFingerProb + scratchHeadProb) {
				anim.SetInteger ("EventNumber", 3);
				stopReaction ();
			} else {
				anim.SetInteger ("EventNumber", 0);
				chooseReaction ();
			}
		} else if (baseCurState.nameHash == idleState0_TapFingerR) {
			diceRoll = Random.Range (0, 100);
			//print ("in idleState0_TapFingerR");
			if (diceRoll < tapFingerLoopProb) {
				anim.SetInteger ("EventNumber", 1);
			} else {
				anim.SetInteger ("EventNumber", 0);
			}
		} else if (baseCurState.nameHash == idleState0_Stretch || baseCurState.nameHash == idleState1_Stretch) {
			diceRoll = Random.Range (0, 100);
			//print ("in idleState0_Stretch or idleState1_Stretch");
			if (diceRoll < unstretch1Prob) {
				anim.SetInteger ("EventNumber", 1);
			} else {
				anim.SetInteger ("EventNumber", 2);
			}
		} else if (baseCurState.nameHash == idleState1) {
			diceRoll = Random.Range (0, 100);
			//print ("in idleState1");
			//Debug.Log(diceRoll);
			if (diceRoll < leanBack1Prob) {
				anim.SetInteger ("EventNumber", 1);
				stopReaction ();
			} else if (diceRoll < stretch1Prob + leanBack1Prob) {
				anim.SetInteger ("EventNumber", 2);
				stopReaction ();
			} else {
				anim.SetInteger ("EventNumber", 0);
				chooseReaction ();
			}
		} else if (baseCurState.nameHash == idleState1_Lean_Loop) {
			diceRoll = Random.Range (0, 100);
			//print ("in idleState1_Lean_Loop");
			if (diceRoll < stopLeanLoopProb) {
				anim.SetInteger ("EventNumber", 0);
			} else {
				anim.SetInteger ("EventNumber", 1);
			}
		} else {
			anim.SetInteger("EventNumber",Random.Range(0,3));
			stopReaction();
		}
	}

	void FixedUpdate() {
		if (GameObject.Find ("God").GetComponent<GODPause> ().isRunning ()) {
			anim.speed = animatorSpeed;
			this.Running ();
		} else {
			anim.speed = 0.0f;
		}
	}
}
