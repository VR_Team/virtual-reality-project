﻿using UnityEngine;
using System.Collections;

public class MenuButtonDispatch : MonoBehaviour {

	public GameObject[] buttons;
	private Color onColor;
	private int onFontSize;
	private Color offColor;
	private int offFontSize;
	public int buttonDefault = 0;
	private int currentButton;
	public int backButton = -1;

	// Use this for initialization
	void Start () {
		if (buttonDefault < 0 || buttonDefault >= buttons.Length) {
			buttonDefault = 0;
		}
		currentButton = buttonDefault;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void restart(){
		turnOffCur ();
		currentButton = buttonDefault;
		turnOnCur ();
	}

	public void init(AssemblyCSharp.ButtonVisualInfo info){
		onColor = info.onColor;
		offColor = info.offColor;
		if (info.onFontSize < 0) {
			onFontSize = buttons [currentButton].GetComponent<TextMesh>().fontSize;
		}
		else
		{
			onFontSize = info.onFontSize;
		}

		if (info.offFontSize < 0) {
			offFontSize = buttons [currentButton].GetComponent<TextMesh>().fontSize;
		}
		else
		{
			offFontSize = info.offFontSize;
		}
	}

	public void doDown() {
		int temp = buttons [currentButton].GetComponent<MenuButtonController> ().doDown ();
		if (temp >= 0) {
			turnOffCur();
			currentButton = temp;
			turnOnCur();
		}
	}

	public void doUp() {
		int temp = buttons [currentButton].GetComponent<MenuButtonController> ().doUp ();
		if (temp >= 0) {
			turnOffCur();
			currentButton = temp;
			turnOnCur();
		}
	}

	public void doLeft() {
		print ("do left");
		int temp = buttons [currentButton].GetComponent<MenuButtonController> ().doLeft ();
		if (temp >= 0) {
			turnOffCur();
			currentButton = temp;
			turnOnCur();
		}
	}

	public void doRight() {
		int temp = buttons [currentButton].GetComponent<MenuButtonController> ().doRight ();
		if (temp >= 0) {
			turnOffCur();
			currentButton = temp;
			turnOnCur();
		}
	}

	public int doEscape() {
		restart ();
		return buttons [backButton].GetComponent<MenuButtonController> ().doReturn ();
	}

	public AssemblyCSharp.ReturnMessage doReturn() {
		AssemblyCSharp.ReturnMessage temp;
		temp.dest = buttons [currentButton].GetComponent<MenuButtonController> ().doReturn ();
		temp.back = backButton;
		if (currentButton == backButton) {
			restart();
		}
		return temp;
	}

	private void turnOnCur() {
		GameObject cur = buttons [currentButton];
		cur.renderer.material.color = onColor;
		cur.GetComponent<TextMesh> ().fontSize = onFontSize;
	}

	private void turnOffCur() {
		GameObject cur = buttons [currentButton];
		cur.renderer.material.color = offColor;
		cur.GetComponent<TextMesh> ().fontSize = offFontSize;
	}

	public int getCurButton(){
		return currentButton;
	}
}
