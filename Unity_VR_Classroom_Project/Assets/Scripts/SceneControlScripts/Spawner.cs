﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	private GameObject curObj;
	private System.Collections.Generic.Stack<GameObject> parentStack;

	void Start() {
		parentStack = new System.Collections.Generic.Stack<GameObject> ();
	}


	public bool LoadGameObject(string prefabName, string objName){
		Object res = Resources.Load (prefabName, typeof(GameObject));
		if (res == null) {
			//If resource is not found
			Debug.LogError ("Unable to find Resource<" + prefabName + ">");
			return false;
		} else if (parentStack.Count > 0) {
			//
			Debug.LogError("Loading New Object before done");
			return false;
		}
		curObj = Instantiate(res) as GameObject;
		curObj.name = objName;
		return true;
	}

	public void SetName(string objName){
		curObj.name = objName;
	}

	public void SetObjPosition(Vector3 pos){
		curObj.transform.position = pos;
	}

	public void SetLocalPosition(Vector3 pos){
		curObj.transform.localPosition = pos;
	}
	
	public void SetObjRotation(Vector3 rot){
		curObj.transform.rotation = Quaternion.Euler(rot);
	}

	public void SetLocalRotation(Vector3 rot){
		curObj.transform.localEulerAngles = rot;
	}

	public void SetScale(Vector3 sca){
		curObj.transform.localScale = sca;
	}

	public void GoToChild(string childName){
		Transform temp = curObj.transform.Find (childName);
		if (temp == null) {
			Debug.LogError(childName + " is not a child of " + curObj.name);
			return;
		}
		parentStack.Push (curObj);
		curObj = temp.gameObject;
	}

	public bool AddChild(string prefabName){
		Object res = Resources.Load (prefabName, typeof(GameObject));
		Transform parentTransform = curObj.transform;
		if (res == null) {
			//If resource is not found
			Debug.LogError("Unable to find Resource<" + prefabName + ">");
			return false;
		}
		parentStack.Push (curObj);
		curObj = Instantiate (res) as GameObject;
		curObj.transform.parent = parentTransform;
		return true;
	}

	public bool EndObj(){
		if (parentStack.Count == 0) {
			Debug.LogError("No Parent for Object " + curObj.name);
			return false;
		}
		curObj = parentStack.Pop ();
		return true;
	}

	public bool IsDone(){
		if (parentStack.Count == 0) {
			return true;
		} 
		else {
			return false;
		}
	}

	public GameObject GetCurrentObjectHandler(){
		return curObj;
	}
	
}
