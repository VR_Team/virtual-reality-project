﻿using UnityEngine;
using System.Collections;

public class GameMenuMessenger : MonoBehaviour {
	public TextMesh NameText;
	public TextMesh IdText;

	private string childName;
	private string childId; 

	// Use this for initialization
	void Start () {
		childName = NameText.text;
		childId = IdText.text;
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		int curLevel = Application.loadedLevel;
		if (curLevel == 0) {
			childName = NameText.text;
			childId = IdText.text;
		} else {
			if(GameObject.Find("God") != null){
				GameObject.Find ("God").GetComponent<GODRecord>().fileNamebase(childName + "." + childId);
				Destroy(this.gameObject);
			}
		}
	}



}
