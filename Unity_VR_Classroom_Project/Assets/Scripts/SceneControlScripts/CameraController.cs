﻿using UnityEngine;
using System.Collections;

public class CameraController: MonoBehaviour {

	public Transform[] screenArr; //Array of transforms to screen objects in scene
	public int[] windowEntries; //Array of integers indicating number on entries in each window
	public string buttonsParentObjName = "Buttons";
	public string cameraPositionObjectName = "CameraPos";

	//Record Variables
	public Color buttonOnColor; //The text color of buttons when selected
	public int buttonOnFontSize = -1;
	public Color buttonOffColor; //The text color of buttons when not selected
	public int buttonOffFontSize = -1;
	private int curScreen; //The current menu state
	public double panSeconds = 0.750; //Time to pan to next window (in seconds)
	private bool panning = false;
	private double timePanning = 0;
	private Vector3 panDirection = new Vector3(0,0,0);
	private Vector3 startCoords;
	private bool disabled;

	// Use this for initialization
	void Start () {
		if (screenArr.Length <= 0) {
			Debug.LogError("No screens defined");
			Application.Quit();
		}
		disabled = false;
		curScreen = 0; //initialize state
		transform.position = screenArr[curScreen].FindChild(cameraPositionObjectName).transform.position; //initalize camera position
		AssemblyCSharp.ButtonVisualInfo buttonInfo = initButtonInfo ();
		for (int i = 0; i < screenArr.Length; i++) {
			GameObject temp = screenArr [i].transform.FindChild (buttonsParentObjName).gameObject;
			temp.GetComponent<MenuButtonDispatch> ().init (buttonInfo);
			temp.GetComponent<MenuButtonDispatch>().restart();
		}
	}
	
	private AssemblyCSharp.ButtonVisualInfo initButtonInfo(){
		AssemblyCSharp.ButtonVisualInfo var;
		var.onColor = buttonOnColor;
		var.offColor = buttonOffColor;
		var.onFontSize = buttonOnFontSize;
		var.offFontSize = buttonOffFontSize;
		return var;
	}

	public void Disable(){
		disabled = true;
	}

	public void Enable(){
		disabled = false;
	}

	// Update is called once per frame
	void Update () {
		if (panning) {
			animateScreenMove ();
			return;
		} else if (disabled) {
			return;
		}

		if (Input.GetKeyDown (KeyCode.DownArrow)) {
			onDownArrowControl ();
		} 
		else if (Input.GetKeyDown (KeyCode.UpArrow)) {
			onUpArrowControl ();
		} 
		else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			onLeftArrowControl();
		} 
		else if (Input.GetKeyDown (KeyCode.RightArrow)) {
			onRightArrowControl();
		}
		else if (Input.GetKeyDown (KeyCode.Return)) {
			onReturnControl ();
		} 
		else if (Input.GetKeyDown (KeyCode.Escape)) {
			onEscapeControl ();
		} 
		else {
		}
	}
	
	void ChangeState(int screenCode) {
		curScreen = screenCode;
	}

	void onDownArrowControl() {
		screenArr [curScreen].transform.FindChild (buttonsParentObjName).gameObject.GetComponent<MenuButtonDispatch> ().doDown ();
	}

	void onUpArrowControl() {
		screenArr [curScreen].transform.FindChild (buttonsParentObjName).gameObject.GetComponent<MenuButtonDispatch> ().doUp ();
	}

	void onLeftArrowControl() {
		screenArr [curScreen].transform.FindChild (buttonsParentObjName).gameObject.GetComponent<MenuButtonDispatch> ().doLeft ();
	}

	void onRightArrowControl() {
		screenArr [curScreen].transform.FindChild (buttonsParentObjName).gameObject.GetComponent<MenuButtonDispatch> ().doRight ();
	}

	void onReturnControl() {
		AssemblyCSharp.ReturnMessage temp = screenArr [curScreen].transform.FindChild (buttonsParentObjName).gameObject.GetComponent<MenuButtonDispatch> ().doReturn ();
		if ( temp.dest >= 0) {
			curScreen = temp.dest;

			if (temp.back > 0 && temp.dest == temp.back) {
				screenArr[curScreen].transform.FindChild(buttonsParentObjName).gameObject.GetComponent<MenuButtonDispatch> ().restart ();
			}

			startCoords = transform.position;
			panDirection = screenArr[curScreen].FindChild(cameraPositionObjectName).transform.position - startCoords;
			panning = true;
			timePanning = 0;
		}

	}

	void onEscapeControl() {
		int temp = screenArr [curScreen].transform.FindChild (buttonsParentObjName).gameObject.GetComponent<MenuButtonDispatch> ().doEscape();
		if (temp >= 0) {
			curScreen = temp;
			startCoords = transform.position;
			panDirection = screenArr[curScreen].FindChild(cameraPositionObjectName).transform.position - startCoords;
			panning = true;
			timePanning = 0;
		}
	}

	void animateScreenMove() {
		timePanning = timePanning + Time.deltaTime;
		if (timePanning < panSeconds) {
			float frac = (float) (timePanning / panSeconds);
			transform.position = startCoords + new Vector3(panDirection.x * frac, panDirection.y * frac, panDirection.z * frac);
		} 
		else {
			transform.position = screenArr[curScreen].FindChild(cameraPositionObjectName).transform.position;
			panning = false;
		}
	}

	public int getCurScreen(){
		return curScreen;
	}

	public bool isPanning(){
		return panning;
	}
}
