﻿using UnityEngine;
using System.Collections;

public class EnableCameraOnPause : MonoBehaviour {

	private GODPause pauseHandler;

	// Use this for initialization
	void Start () {
		pauseHandler = GameObject.Find ("God").GetComponent<GODPause> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (pauseHandler.isPaused ()) {
			this.camera.enabled = true;
		} else if (pauseHandler.isRunning ()) {
			this.camera.enabled = false;
		}
	}
}
