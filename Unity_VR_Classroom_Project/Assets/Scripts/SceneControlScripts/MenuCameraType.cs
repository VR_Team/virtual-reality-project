﻿using UnityEngine;
using System.Collections;

public class MenuCameraType : MonoBehaviour {

	public GameObject CameraHolder;
	public GameObject ButtonGroup;
	public MenuButtonController SimulationSceneScriptHandle;
	public int triggerScreen;
	public int triggerButton;
	public TextMesh cameraTypeText;

	private CameraController cameraControlScriptHandle;
	private MenuButtonDispatch buttonDispatchScriptHandle;

	private const int normalSceneIndex = 1;
	private const int oculusSceneIndex = 2;

	// Use this for initialization
	void Start () {
		cameraControlScriptHandle = CameraHolder.GetComponent<CameraController> ();
		buttonDispatchScriptHandle = ButtonGroup.GetComponent<MenuButtonDispatch> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (cameraControlScriptHandle.getCurScreen () == triggerScreen && 
		    	!cameraControlScriptHandle.isPanning() && 
		    	buttonDispatchScriptHandle.getCurButton () == triggerButton && 
		    	Input.GetKeyDown(KeyCode.Return)){
			if(cameraTypeText.text == "Normal Camera"){
				cameraTypeText.text = "Oculus Camera";
				SimulationSceneScriptHandle.dest = oculusSceneIndex;
			}
			else {
				cameraTypeText.text = "Normal Camera";
				SimulationSceneScriptHandle.dest = normalSceneIndex;
			}
		}
	}
}
