﻿using UnityEngine;
using System.Collections;

public class MenuButtonController : MonoBehaviour {

	public bool closeApp; //if set button will close application.
	public bool sceneChange; //if set (and closeApp is not set) button will load a new scene.
	public int dest = -1; //which state/scene to change to on select

	public int rightButton = -1; //specifies which button to change to when right arrow is pressed
	public int downButton = -1; //specifies which button to change to when down arrow is pressed
	public int upButton = -1; //specifies which button to change to when up arrow is pressed
	public int leftButton = -1; //specifies which button to change to when left arrow is pressed
	//Negative value indicates no change.

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public int doDown() {
		return downButton;
	}

	public int doUp() {
		return upButton;
	}

	public int doLeft() {
		return leftButton;
	}

	public int doRight() {
		return rightButton;
	}

	public int doReturn() {
		if (closeApp) {
			Application.Quit ();
		} 
		else if (sceneChange && dest > 0) {
			Application.LoadLevel(dest);
		}
		return dest;
	}
}
