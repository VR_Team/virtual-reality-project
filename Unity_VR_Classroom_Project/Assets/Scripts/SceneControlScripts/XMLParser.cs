﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.IO;

public class XMLParser : MonoBehaviour {
	private System.Collections.Generic.Stack<string> elementStack;
	private Spawner spawner;
	private XmlReader reader;
	private SceneObjectNames sceneNames;
	private const string simDir = "/Simulations";
	public string path;

	public void ParseXML (string filePath){
		reader = XmlReader.Create (filePath);

		reader.MoveToContent ();
		do {
			XmlReaderSkip();

			if(reader.NodeType == XmlNodeType.Element){
				ElementDispatch(reader.Name);
				elementStack.Push(reader.Name);
				//print("Start: " + reader.Name);
			}
			else if(reader.NodeType == XmlNodeType.EndElement){
				elementStack.Pop ();
				//print ("End " + reader.Name);
				EndElementDispatch(reader.Name);
			}
			else if(reader.NodeType == XmlNodeType.None || reader.NodeType == XmlNodeType.Text){
				//do Nothing
			}
			else {
				//Unexpected Node Type
				Debug.Log ("Unregistered Node Type: " + reader.NodeType);
			}
			reader.Read();
		} while(!reader.EOF);
		reader.Close ();
	}

	private void XmlReaderSkip(){
		//Skip comments and whitespace
		while(reader.NodeType == XmlNodeType.Whitespace || 
		      reader.NodeType == XmlNodeType.Comment){
			reader.Read();
		}
	}

	private void LogUnexpectedParentElement(string eleName){
		Debug.LogError("Element \"" + eleName + "\" childed under unexpected parent element " + elementStack.Peek());
	}

	private void LogMissingAttribute(string ele, string attrib){
		Debug.LogError ("Element \"" + ele + "\" missing attribute \"" + attrib +"\"");
	}

	private void EndElementDispatch(string elementName){
		if (elementStack.Count > 0 && 
		    	(elementStack.Peek () == "gameobject" || elementStack.Peek () == "child") && 
		    	(elementName == "gameobject") || (elementName == "child")) {
			spawner.EndObj();
		}
	}

	private void LogInvalidComponent(string componentName, string objectName){
		Debug.LogError("Cannot attach Component <StudentControllerScript1> to Object <" + spawner.GetCurrentObjectHandler().name + ">");
	}
	
	private void ElementDispatch(string elementName){
		//Switch to handle element types
		switch (elementName){
			case("gameobject"):
				if(elementStack.Peek() == "child" || elementStack.Peek() == "gameobject"){
					AppendGameObject();
				}
				else if (elementStack.Peek () == "simulation"){
					MakeGameObject();
				}
				else {
					LogUnexpectedParentElement(elementName);
				}
				break;
			case("position"):
				if(elementStack.Peek () == "child" || elementStack.Peek() == "gameobject"){
					SetObjectPosition();
				}
				else if (elementStack.Peek() == "camera") {
					SetCameraPosition();
				} 
				else {
					LogUnexpectedParentElement(elementName);
				}
				break;
			case("rotation"):
				if(elementStack.Peek () == "child" || elementStack.Peek () == "gameobject"){
					SetObjectRotation();
				}
				else if (elementStack.Peek() == "camera"){
					SetCameraRotation();
				}
				else {
					LogUnexpectedParentElement(elementName);
				}
				break;
			case("scale"):
				if(elementStack.Peek () == "child" || elementStack.Peek () == "gameobject"){
					SetObjectScale();
				}
				else {
					LogUnexpectedParentElement(elementName);
				}
				break;
			case("child"):
				if(elementStack.Peek () == "child" || elementStack.Peek () == "gameobject"){
					HandleChild();
				}
				else {
					LogUnexpectedParentElement(elementName);
				}
				break;
			case("studentcontrollerscript1"):
				if(elementStack.Peek () == "gameobject" || elementStack.Peek() == "child"){
					if(spawner.GetCurrentObjectHandler().GetComponent<StudentControllerScript1>() == null){
						LogInvalidComponent("studentcontrollerscript1", spawner.GetCurrentObjectHandler().name);
					}
					else{
						HandleSCS1();
					}
				}
				break;
			case("clockanimatorscript"):
				if(elementStack.Peek () == "gameobject" || elementStack.Peek() == "child"){
					if(spawner.GetCurrentObjectHandler().GetComponent<ClockAnimator>() == null){
						LogInvalidComponent("clockanimatorscript", spawner.GetCurrentObjectHandler().name);
					}
					else {
						HandleClockAnimatorScript();
					}
				}
			break;
			case("mathtaskscript"):
				if(elementStack.Peek () == "gameobject" || elementStack.Peek() == "child"){
					if(spawner.GetCurrentObjectHandler().GetComponent<MathTask>() == null){
						LogInvalidComponent("mathtaskscript", spawner.GetCurrentObjectHandler().name);
					}
					else {
						HandleMathTaskScript();
					}
				}
				break;
			case("recordlookedatscript"):
				if(elementStack.Peek() == "gameobject" || elementStack.Peek() == "child"){
					HandleRecordLookedAtScript();
				}
				break;
			default:
				break;
		}
	}

	private void HandleRecordLookedAtScript(){
		spawner.GetCurrentObjectHandler ().AddComponent<RecordLookedAt> ();
	}

	private void HandleMathTaskScript (){
		MathTask scriptHandle = spawner.GetCurrentObjectHandler ().GetComponent<MathTask> ();
		string delayTime, startTime, endTime, timeOut, probNum,fileName,scramble;
		delayTime = reader.GetAttribute ("delaytime");
		startTime = reader.GetAttribute ("starttime");
		endTime = reader.GetAttribute ("endtime");
		timeOut = reader.GetAttribute ("timeout");
		probNum = reader.GetAttribute ("probnum");
		fileName = reader.GetAttribute ("filename");
		scramble = reader.GetAttribute ("scramble");

		if (delayTime != null) {
			scriptHandle.problemDelay = float.Parse(delayTime);
		}
		if (startTime != null) {
			scriptHandle.start_time = float.Parse(startTime);
		}
		if (endTime != null) {
			scriptHandle.end_time = float.Parse(endTime);
		}
		if (timeOut != null) {
			scriptHandle.time_out = float.Parse(timeOut);
		}
		if (probNum != null) {
			scriptHandle.max_question = int.Parse(probNum);
		}
		if (fileName != null) {
			scriptHandle.problemFileName = fileName;
		}
		if (scramble == "true") {
			scriptHandle.scramble = true;
		}
	}

	private void HandleClockAnimatorScript(){
		ClockAnimator scriptHandle = spawner.GetCurrentObjectHandler ().GetComponent<ClockAnimator> ();
		string playTickingSound, tickPitch, tickVolume, tickDelayCounter,
				startTime;
		playTickingSound = reader.GetAttribute ("ticking");
		tickPitch = reader.GetAttribute ("tickpitch");
		tickVolume = reader.GetAttribute ("tickvolume");
		tickDelayCounter = reader.GetAttribute("tickdelaycounter");
		startTime = reader.GetAttribute ("starttime");

		if (playTickingSound != null) {
			if(playTickingSound == "true"){
				scriptHandle.playTickingSound = true;
			}
			else if(playTickingSound == "false"){
				scriptHandle.playTickingSound = false;
			}
			if(tickPitch != null){
				scriptHandle.tickPitch = float.Parse(tickPitch);
			}
			if(tickVolume != null){
				scriptHandle.tickVolume = float.Parse(tickVolume);
			}
			if(tickDelayCounter != null){
				scriptHandle.tickDelayCounter = int.Parse (tickDelayCounter);
			}
		}
		if (startTime != null) {
			string[] times = startTime.Split(':');
			scriptHandle.hourVal = int.Parse(times[0]);
			scriptHandle.minuteVal = int.Parse(times[1]);
			scriptHandle.secondVal = int.Parse (times[2]);
		}
	}

	private void HandleSCS1(){
		StudentControllerScript1 scriptHandle = spawner.GetCurrentObjectHandler().GetComponent<StudentControllerScript1>();
		string animSpeed, sneezePitch, sneezeVolume, rollsPerSec,
				sneezeProb, talkProb,
				tapFingerProb, scratchHeadProb, stretch0Prob,
				tapFingerLoopProb,
				unstretch1Prob,
				leanBack1Prob, stretch1Prob,
				stopLeanLoopProb;

		animSpeed = reader.GetAttribute ("animatorspeed");
		sneezePitch = reader.GetAttribute ("sneezepitch");
		sneezeVolume = reader.GetAttribute ("sneezevolume");
		rollsPerSec = reader.GetAttribute ("dicerollspersecond");
		sneezeProb = reader.GetAttribute ("sneezestateprobability");
		talkProb = reader.GetAttribute ("talkstateprobability");
		tapFingerProb = reader.GetAttribute ("startfingertapprobability");
		scratchHeadProb = reader.GetAttribute ("scratchheadprobability");
		stretch0Prob = reader.GetAttribute ("stretchfrom0probability");
		tapFingerLoopProb = reader.GetAttribute ("loopfingertapprobability");
		unstretch1Prob = reader.GetAttribute ("unstretchto1probability");
		leanBack1Prob = reader.GetAttribute ("leanbackfrom1probability");
		stretch1Prob = reader.GetAttribute ("stretchfrom1probability");
		stopLeanLoopProb = reader.GetAttribute ("breakleanbackloopprobability");

		if (animSpeed != null) {
			scriptHandle.animatorSpeed = float.Parse(animSpeed);
		}
		if (sneezePitch != null) {
			scriptHandle.sneezePitch = float.Parse(sneezePitch);
		}
		if (sneezeVolume != null) {
			scriptHandle.sneezeVolume = float.Parse(sneezeVolume);
		}
		if (rollsPerSec != null) {
			scriptHandle.rollsPerSecond = float.Parse(rollsPerSec);
		}
		if (sneezeProb != null) {
			scriptHandle.sneezeProb = float.Parse(sneezeProb);
		}
		if (talkProb != null) {
			scriptHandle.talkProb = float.Parse(talkProb);
		}
		if (tapFingerProb != null) {
			scriptHandle.tapFingerProb = float.Parse(tapFingerProb);
		}
		if (scratchHeadProb != null) {
			scriptHandle.scratchHeadProb = float.Parse(scratchHeadProb);
		}
		if (stretch0Prob != null) {
			scriptHandle.stretch0Prob = float.Parse(stretch0Prob);
		}
		if (tapFingerLoopProb != null) {
			scriptHandle.tapFingerLoopProb = float.Parse(tapFingerLoopProb);
		}
		if (unstretch1Prob != null) {
			scriptHandle.unstretch1Prob = float.Parse(unstretch1Prob);
		}
		if (leanBack1Prob != null) {
			scriptHandle.leanBack1Prob = float.Parse(leanBack1Prob);
		}
		if (stretch1Prob != null) {
			scriptHandle.stretch1Prob = float.Parse(stretch1Prob);
		}
		if (stopLeanLoopProb != null) {
			scriptHandle.stopLeanLoopProb = float.Parse(stopLeanLoopProb);
		}
	}

	private void HandleChild(){
		string name, newName;
		name = reader.GetAttribute ("name");
		newName = reader.GetAttribute ("newname");
		if (name == null) {
			LogMissingAttribute ("child", "name");
			return;
		} else if (newName == null) {
			LogMissingAttribute("child", "newname");
		} else {
			if(sceneNames.NameIsAvailable(newName)){
				spawner.GoToChild (name);
				spawner.SetName(newName);
			} else {
				Debug.LogError("GameObject name \"" + name + "\" is reserved or in use");
				return;
			}
		}
	}

	private void SetObjectScale(){
		string value;
		Vector3 valVec = new Vector3 ();
		value = reader.GetAttribute ("val");
		if (value == null) {
			LogMissingAttribute ("scale", "val");
			return;
		} else {
			string[] split_val = value.Split(',');
			if(split_val.Length == 3) {
				valVec.Set(float.Parse(split_val[0]), float.Parse(split_val[1]), float.Parse(split_val[2]));
			}
			else {
				LogBadAttribute("scale", "val", value);
				return;
			}
		}
		
		spawner.SetScale (valVec);
	}

	private void SetCameraRotation (){
		string value;
		Vector3 valVec = new Vector3 ();
		value = reader.GetAttribute ("val");
		if (value == null) {
			LogMissingAttribute ("rotation", "val");
			return;
		} else {
			string[] split_val = value.Split(',');
			if(split_val.Length == 3) {
				valVec.Set(float.Parse(split_val[0]), float.Parse(split_val[1]), float.Parse(split_val[2]));
			}
			else {
				LogBadAttribute("rotation", "val", value);
				return;
			}
		}

		GameObject.Find ("MainCamera").transform.rotation = Quaternion.Euler(valVec);
	}

	private void SetObjectRotation (){
		string value, type;
		Vector3 valVec = new Vector3 ();
		value = reader.GetAttribute ("val");
		type = reader.GetAttribute ("type");
		if (value == null) {
			LogMissingAttribute ("rotation", "val");
			return;
		} else {
			string[] split_val = value.Split(',');
			if(split_val.Length == 3) {
				valVec.Set(float.Parse(split_val[0]), float.Parse(split_val[1]), float.Parse(split_val[2]));
			}
			else {
				LogBadAttribute("rotation", "val", value);
				return;
			}
		}
		
		if (type == null || type == "global") {
			spawner.SetObjRotation(valVec);
		}
		else if (type == "local") {
			spawner.SetLocalRotation(valVec);
		}
		else {
			LogBadAttribute("rotation", "type", type);
			return;
		}
	}

	public void SetCameraPosition(){
		string value;
		Vector3 valVec = new Vector3 ();
		value = reader.GetAttribute ("val");
		if (value == null) {
			LogMissingAttribute ("position", "val");
			return;
		} else {
			string[] split_val = value.Split(',');
			if(split_val.Length == 3) {
				valVec.Set(float.Parse(split_val[0]), float.Parse(split_val[1]), float.Parse(split_val[2]));
			}
			else {
				LogBadAttribute("position", "val", value);
				return;
			}
		}

		GameObject.Find ("MainCamera").transform.position = valVec;
	}

	private void SetObjectPosition (){
		string value, type;
		Vector3 valVec = new Vector3 ();
		value = reader.GetAttribute ("val");
		type = reader.GetAttribute ("type");
		if (value == null) {
			LogMissingAttribute ("position", "val");
			return;
		} else {
			string[] split_val = value.Split(',');
			if(split_val.Length == 3) {
				valVec.Set(float.Parse(split_val[0]), float.Parse(split_val[1]), float.Parse(split_val[2]));
			}
			else {
				LogBadAttribute("position", "val", value);
				return;
			}
		}

		if (type == null || type == "global") {
			spawner.SetObjPosition(valVec);
		}
		else if (type == "local") {
			spawner.SetLocalPosition(valVec);
		}
		else {
			LogBadAttribute("position", "type", type);
			return;
		}
	}

	private void LogBadAttribute(string ele, string attrib, string attribVal) {
		Debug.LogError ("Bad value (" + attribVal + ") for " + ele + " attribute " + attrib);
	}

	private void MakeGameObject(){
		string name, source;
		name = reader.GetAttribute ("name");
		source = reader.GetAttribute ("src");
		if (name == null) {
			LogMissingAttribute("gameobject","name");
			return;
		} else if (source == null) {
			LogMissingAttribute("gameobject","src");
			return;
		}

		if (sceneNames.NameIsAvailable (name)) {
			spawner.LoadGameObject (source, name);
		} else {
			Debug.LogError("Root GameObject name \"" + name + "\" is reserved or in use");
			return;
		}
	}

	private void AppendGameObject(){
		string name, source;
		name = reader.GetAttribute ("name");
		source = reader.GetAttribute ("src");
		if (name == null) {
			LogMissingAttribute("gameobject","name");
			return;
		} else if (source == null) {
			LogMissingAttribute("gameobject","src");
			return;
		}

		if (sceneNames.NameIsAvailable (name)) {
			print ("New obj " + source + " __ " + name);
			spawner.AddChild (source);
			spawner.SetName(name);
		} else {
			Debug.LogError("GameObject name \"" + name + "\" is reserved or in use");
			return;
		}
	}

	// Use this for initialization
	void Start () {
		elementStack = new System.Collections.Generic.Stack<string> ();
		sceneNames = new SceneObjectNames ();
		spawner = this.GetComponent<Spawner>();
		if (!Directory.Exists (Application.dataPath + simDir)) {
			Directory.CreateDirectory(Application.dataPath + simDir);
		}

		path = Application.dataPath + simDir + "/" + path;
		ParseXML (path);
	}
}
