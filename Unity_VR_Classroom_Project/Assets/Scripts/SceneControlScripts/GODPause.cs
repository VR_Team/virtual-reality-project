﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.IO;

public class GODPause : MonoBehaviour {
	private bool isPause;
	private bool isRun;
	private bool exitStat;
	private GameObject pCam;
	public GameObject pauseMarker;
	public float markerDist;
	public float unpauseAccuracy;
	private Vector3 storePos;
	private AssemblyCSharp.DataMessage msg;
	private GODRecord recordScriptHandle;



	private float totalTime; // Time since simulation scene started
	private float simTime; // Time that simulation scene is in running state
	private float pauseTime; // Time that simuation scene is in pause state
	private float unpausingTime; // Time that simulation scene is in unpausing state

	// Use this for initialization
	void Start () {
		pCam = GameObject.Find ("PauseCamera");
		recordScriptHandle = GameObject.Find ("God").GetComponent<GODRecord> ();
		isPause = false;
		isRun = true;
		pCam.camera.enabled = false;
		exitStat = false;

		totalTime = 0.0f;
		simTime = 0.0f;
		pauseTime = 0.0f;
		unpausingTime = 0.0f;
	}
	void Update(){
		totalTime += Time.deltaTime;

		if (isRun) {
			simTime += Time.deltaTime;
		}
		else if (isPause) {
			pauseTime += Time.deltaTime;
		}
		else {
			unpausingTime += Time.deltaTime;
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			if(isPause){
				pCam.camera.enabled = true;
				exitStat = false;
				if(!isRun){
					isPause = false;

				}
				else{

				}
			}
			else{
				if(isRun){
					storePos = pCam.transform.forward;
					pauseMarker.transform.position = pCam.transform.position + storePos * markerDist;

					msg = new AssemblyCSharp.DataMessage();
					msg.objectName = this.gameObject.name;
					msg.data = "paused:" + totalTime;
					msg.messageType = MACROS.Constants.DATAMESSAGE_PAUSE;
					recordScriptHandle.storeMsg(msg);
				}
				isPause = true;
				isRun = false;
				pCam.camera.enabled = true;

				msg = new AssemblyCSharp.DataMessage();
				msg.objectName = this.gameObject.name;
				msg.data = "unpausing:" + totalTime;
				msg.messageType = MACROS.Constants.DATAMESSAGE_PAUSE;
				recordScriptHandle.storeMsg(msg);
			}
		}
		if(!isPause && !isRun){
			if ((Vector3.Dot(storePos,pCam.transform.forward)+ 1) / 2.0 >= unpauseAccuracy){
				pCam.camera.enabled = false;
				isRun = true;

				msg = new AssemblyCSharp.DataMessage();
				msg.objectName = this.gameObject.name;
				msg.data = "running:" + totalTime;
				msg.messageType = MACROS.Constants.DATAMESSAGE_PAUSE;
				recordScriptHandle.storeMsg (msg);
			}
		}

	}

	public float getTotalTime(){
		return totalTime;
	}

	public float getSimTime(){
		return simTime;
	}

	public float getUnpausingTime(){
		return unpausingTime;
	}

	public bool isPaused() {
		return isPause;
	}

	public bool isRunning() {
		return isRun;
	}

	void OnGUI() {
			if (isPause && exitStat == false) {
				GUI.BeginGroup(new Rect(Screen.width/2 - 50, Screen.height/2 - 50, 100, 100));
				GUI.Box(new Rect(0,0,100,100),"Paused");
	//			GUI.EndGroup();

				if (GUI.Button (new Rect(10,40,80,30),"Quit")) {
					exitStat = true;
				}
			GUI.EndGroup();
			}
		if (isPause && exitStat == true) {
				GUI.BeginGroup (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));
				GUI.Box (new Rect (0, 0, 100, 100), "Are you sure?");
				//			GUI.EndGroup();
			
				if (GUI.Button (new Rect (10, 25, 80, 30), "Yes")) {
					Application.LoadLevel(0);
				}
				if (GUI.Button (new Rect (10, 60, 80, 30), "No")) {
					exitStat = false;
				}
				GUI.EndGroup ();
				}
		}
}