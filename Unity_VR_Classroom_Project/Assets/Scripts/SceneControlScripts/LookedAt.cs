﻿using UnityEngine;
using System.Collections;

public class LookedAt : MonoBehaviour 
{
	private GODPause pauseController;
	private GameObject userCamera;
	public static Collider collided = new Collider();
	
	// Use this for initialization
	void Start () 
	{
		userCamera = GameObject.Find ("MainCamera");
		pauseController = GameObject.Find ("God").GetComponent<GODPause> ();
	}

	private void run(){
		float length = 20.0f;
		RaycastHit hit;
		Vector3 rayDirection = transform.TransformDirection(Vector3.forward);
		Vector3 rayStart = transform.position + rayDirection;      // Start the ray away from the player to avoid hitting itself
		Debug.DrawRay(rayStart, rayDirection * length, Color.green);
		if (Physics.Raycast(rayStart, rayDirection, out hit, length))
		{
			collided = hit.collider;
			if(collided.gameObject.GetComponent<RecordLookedAt>() != null){
				print("estago");
				collided.gameObject.GetComponent<RecordLookedAt>().SetLookingAt();
			}
		}
	}

	// Update is called once per frame
	void Update () 
	{
		run ();
	}
}
