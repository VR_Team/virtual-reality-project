﻿using UnityEngine;
using System.Collections;

public class MenuInfoStringCam : MonoBehaviour {

	public GameObject CameraHolder;
	public GameObject ButtonGroup;
	public int triggerScreen;
	public int triggerButton;
	public TextMesh InfoText;
	public Camera infoCamera;

	private CameraController cameraControlScriptHandle;
	private MenuButtonDispatch buttonDispatchScriptHandle;
	private bool activeFlag;

	// Use this for initialization
	void Start () {
		cameraControlScriptHandle = CameraHolder.GetComponent<CameraController> ();
		buttonDispatchScriptHandle = ButtonGroup.GetComponent<MenuButtonDispatch> ();
		infoCamera.enabled = false;
		activeFlag = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (cameraControlScriptHandle.getCurScreen () != triggerScreen) {
			infoCamera.enabled = false;
		}

		if (cameraControlScriptHandle.getCurScreen () == triggerScreen && !cameraControlScriptHandle.isPanning()) {
			infoCamera.enabled = true;
			if(buttonDispatchScriptHandle.getCurButton () == triggerButton && Input.GetKeyDown(KeyCode.Return) && !activeFlag){
				cameraControlScriptHandle.Disable();
				activeFlag = true;
				InfoText.color = cameraControlScriptHandle.buttonOnColor;
			}
			else if (activeFlag && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Return))){
				infoCamera.enabled = false;
				cameraControlScriptHandle.Enable();
				activeFlag = false;
				InfoText.color = cameraControlScriptHandle.buttonOffColor;
			}
			else if (activeFlag){
				HandleInput();
			}
		}
	}

	private void HandleInput(){
		string userInput = Input.inputString;
		foreach(char c in userInput.ToCharArray()){
			if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || "\'._,`-".Contains("" + c)){
				InfoText.text += c;
			}
			else if (c == '\b') {
				if(InfoText.text.Length > 0){
					InfoText.text = InfoText.text.Substring(0,InfoText.text.Length - 1);
				}
			}
		}
	}
}
