﻿using UnityEngine;
using System.Collections.Generic;

public class SceneObjectNames {
	private List<string> sceneObjectNames;

	public SceneObjectNames() {
		//Default Scene Objects
		sceneObjectNames = new List<string> ();
		sceneObjectNames.Add ("MainCamera");
		sceneObjectNames.Add ("God");
		sceneObjectNames.Add ("PauseCamera");
		sceneObjectNames.Add ("PauseMarker");
		sceneObjectNames.Add ("Sun");
	}

	public bool NameIsAvailable(string name){
		if (sceneObjectNames.Contains (name)) {
			//if root object name already exists, returns false
			return false;
		} else {
			//if root object name does not exist, adds name to list and returns true
			sceneObjectNames.Add(name);
			return true;
		}
	}
}
