using UnityEngine;
using System.Collections;

namespace MACROS {
	public static class Constants {
		public const int degrees180 = 180;
		public const int degrees360 = 360;
		public const int DATAMESSAGE_NUMTYPES = 5;
		public const int DATAMESSAGE_MATHTASK = 0;
		public const int DATAMESSAGE_LOOKINGAT = 1;
		public const int DATAMESSAGE_STUDENTANIMATIONEVENT = 2;
		public const int DATAMESSAGE_HEADTRACKING = 3;
		public const int DATAMESSAGE_PAUSE = 4;
	}
}