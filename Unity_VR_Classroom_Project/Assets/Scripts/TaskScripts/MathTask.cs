using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.IO;

public class MathTask : MonoBehaviour 
{
	public float problemDelay;
	
	public float start_time = 0.0f; //time math task starts 
	public float end_time = 30.0f; //time math task ends
	public float time_out = 10.0f; //how long a question stays visible before next question
									//same as problemDelay?
	public int max_question = 2; //max # of questions

	public string problemFileName = "mathProblem.csv";
	public  bool scramble = false;
	private string directoryPath = (Application.dataPath + "/MathProblems");

	private AssemblyCSharp.DataMessage msg;
	private GODPause pauseController;
	private GODRecord recordScriptHandle;
	private bool startTaskFlag = false;

	private bool test;
	private bool boolin;
	private float numerator;
	private float denominator = 1.0f;
	private float ratio;
	
	private int question_number = 0; //tracks questions user answered
	private float problemTime = 0.0f; //tracks time for each problem
	private string math_problem; //complete question

	public List<List<String>> filedata;
	
	private void loadProblem()
	{
		if (question_number >= filedata.Count) 
		{
			taskEndMessage();
			Destroy(gameObject);
			return;
		}
			
		while (filedata[question_number].Count != 2 && filedata.ToArray()[question_number].Count != 3) 
		{
			question_number++;
			if (question_number >= filedata.Count) {
				taskEndMessage();
				Destroy(gameObject);
				return;
			}
		}

		math_problem = filedata[question_number][0]; 
		test = Convert.ToBoolean(filedata[question_number][1]);
		if (filedata [question_number].Count == 3) 
			time_out = float.Parse (filedata [question_number] [2]);

		GetComponent<TextMesh> ().text = math_problem; //prints math problem on chalkboard

		question_number++; //increment question counter variable
	}

	void Start() 
	{
		GetComponent<TextMesh> ().text = "";
		pauseController = GameObject.Find ("God").GetComponent<GODPause>();
		recordScriptHandle = GameObject.Find ("God").GetComponent<GODRecord> ();

		if (start_time < 0) 
		{
			start_time = 0;
		}
		if (!Directory.Exists (directoryPath)) 
		{
			Directory.CreateDirectory(directoryPath);
		}
		
		try
		{
			using (StreamReader sr = new StreamReader(directoryPath + "/" + problemFileName))
			{
				filedata = sr.ReadToEnd().Split('\n').Select(s=>s.Split(',').ToList()).ToList();
			}	
		}
		catch(Exception e)
		{
			//Some Error
		}

		if (scramble) {
			ScrambleProblemSet();
		}
	}

	private void run()
	{
		problemTime += Time.deltaTime;

		if (problemTime < problemDelay) 
		{
			return;
		}

		//user did not complete one question and time is up 
		if(problemTime >= time_out && time_out >= 0)
		{
			msg = new AssemblyCSharp.DataMessage();
			omitMakeAndSendMessage();

			//TODO: send message to God.GetComponent<DataRecord>();

			loadProblem();
			problemTime = 0;
			return;
		}

		if (Input.GetKeyDown(KeyCode.Alpha1)) 
		{
			boolin = true; //user answers true

			if (boolin == test) //if user answers true and test is true
			{
				numerator += 1.0f; //user gets 1 point b/c user answered true 
			}
			//else user gets no point b/c user answered false

			ratio = numerator / denominator; 
			denominator += 1.0f; //increment for next problem ex. 1/1 -> 1/2

			//start message making
			answerMakeAndSendMessage();

			loadProblem();
			problemTime = 0.0f; //reset to 0 for next problem
		}
		
		if (Input.GetKeyDown(KeyCode.Alpha2)) 
		{
			boolin = false; //user answers false

			if (boolin == test) //if user answers false test is false 
			{
				numerator += 1.0f; //user gets 1 point b/c user answered false
			}
			//else user gets no point b/c user answered true

			ratio = numerator / denominator;

			denominator += 1.0f; //increment for next problem ex. 1/1 -> 1/2

			//start message making
			answerMakeAndSendMessage();

			loadProblem();
			problemTime = 0.0f; //reset to 0 for next problem
		}
	}

	private void omitMakeAndSendMessage() {

		msg = new AssemblyCSharp.DataMessage();
		msg.objectName = this.gameObject.name;
		msg.data = pauseController.getTotalTime() + "," + math_problem + "," + test + ",,";
		msg.messageType = MACROS.Constants.DATAMESSAGE_MATHTASK;
		recordScriptHandle.storeMsg (msg);
	}

	private void answerMakeAndSendMessage(){
		msg = new AssemblyCSharp.DataMessage();
		msg.objectName = this.gameObject.name;
		msg.data = pauseController.getTotalTime() + "," + math_problem + "," + test + "," + boolin + "," + problemTime;
		msg.messageType = MACROS.Constants.DATAMESSAGE_MATHTASK;
		recordScriptHandle.storeMsg (msg);
	}

	void Update() 
	{
		if (pauseController.GetComponent<GODPause> ().isRunning ()) 
		{
			if(pauseController.getSimTime() >= start_time && (pauseController.getSimTime() < end_time || end_time < 0) && (question_number <= max_question || max_question < 0))
			{
				if(!startTaskFlag){
					taskStartMessage();
					loadProblem();
					startTaskFlag = true;
				}
				this.run ();
			}

			if((pauseController.getSimTime() >= end_time && end_time >= 0) || (question_number > max_question && max_question >= 0))
			{
				taskEndMessage();
				Destroy(gameObject);
			}
		}
	}

	private void taskStartMessage()
	{
		msg = new AssemblyCSharp.DataMessage ();
		msg.objectName = this.gameObject.name;
		msg.data = pauseController.getTotalTime() + ",start";
		msg.messageType = MACROS.Constants.DATAMESSAGE_MATHTASK;
		recordScriptHandle.storeMsg (msg);	}

	private void taskEndMessage()
	{
		msg = new AssemblyCSharp.DataMessage ();
		msg.objectName = this.gameObject.name;
		msg.data = pauseController.getTotalTime() + ",end";
		msg.messageType = MACROS.Constants.DATAMESSAGE_MATHTASK;
		recordScriptHandle.storeMsg (msg);	}

	void Example() 
	{
		renderer.enabled = true;
	}

	public void ScrambleProblemSet(){
		for (int i = 0; i < filedata.Count; i++) {
			int r = UnityEngine.Random.Range(i,filedata.Count);
			List<string> temp = filedata[i];
			filedata[i] = filedata[r];
			filedata[r] = temp;
		}
	}
}