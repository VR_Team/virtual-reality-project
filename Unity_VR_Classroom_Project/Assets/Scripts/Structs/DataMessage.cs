﻿using UnityEngine;
using System.Collections;

namespace AssemblyCSharp
{
	public struct DataMessage
	{
		public string objectName;
		public string data;
		public int messageType;
	}
}