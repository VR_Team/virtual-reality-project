﻿using UnityEngine;
using System.Collections;

public class LookAround : MonoBehaviour {

	private bool LookMode;

	public float xRotationSensitivity = 50;
	public float yRotationSensitivity = 50;
	public float angleUp = 90;

	// Use this for initialization
	void Start () {
		LookMode = false;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (1)) {
			Screen.lockCursor = true;
			LookMode = true;
			Screen.showCursor = false;
		} else if (Input.GetMouseButtonUp (1)) {
			Screen.lockCursor = false;
			LookMode = false;
			Screen.showCursor = true;
		}

		if (LookMode) {
			float dx = Input.GetAxis("Mouse X");
			float dy = Input.GetAxis("Mouse Y");
			this.transform.RotateAround(this.transform.position, Vector3.up, xRotationSensitivity * Time.deltaTime * dx);
			this.transform.Rotate(-Vector3.right * yRotationSensitivity * Time.deltaTime * dy);
		}
	}

	void FixedUpdate() {

	}
}
