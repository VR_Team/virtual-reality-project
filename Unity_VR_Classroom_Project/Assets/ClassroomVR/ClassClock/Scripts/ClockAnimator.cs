﻿using UnityEngine;
using System;

public class ClockAnimator : MonoBehaviour {
	public float triggerAccuracy; //The minimal Accuracy (range: 0-1) that the camera must be facing the object's 
									//local origin in order to decide if the user is looking at the object.
									//0 indicates that the camera is facing the complete opposite direction from
									//the object while 1 indicates that the user is looking directly at the
									//object's origin point.
	public bool playTickingSound = true; //Can be used to enable/disable ticking sound from playing
	public AudioClip tickingSound; //Reference to the audio clip that will be played to simulate the ticking 
									//of the clock's second hand
	public float tickPitch = 1.0f;
	public float tickVolume = 1.0f;
	public int tickDelayCounter; //Integer indicating how many ticks to skip after scene starts.  This helps
									//prevent the audio clip from being played multiple times during initialization.
									//Negative values are treated as 0.
	private Transform seconds, minutes, hours; //References to the transform componenets of the clock hand objects.
											//The clock hands are children to the clock gameObject.
	private GameObject pauseController; //Reference to the object that handles pausing functionality for the scene.
	public int hourVal;
	public int minuteVal;
	public int secondVal;

	private GameObject userCamera;
	private float timeLookedAt; //Used to record the total time that the user has faced the camera
	private float timeCount;
	
	private const float
		hoursToDegrees = 360f / 12f,	//Ratios to convert time values to clock hand angles.
		minutesToDegrees = 360f / 60f,
		secondsToDegrees = 360f / 60f,
		minHourFrac = 1f/60f,
		secMinFrac = 1f/60f,
		secHourFrac = minHourFrac * secMinFrac;

	private bool lookingAtClock = false; //boolean value to indicate that user's camera is looking at the clock
	private bool tickFlag = false;

	//called when the object containing the script is instanciated
	void Start() {
		this.GetComponent<AudioSource> ().pitch = tickPitch;
		this.GetComponent<AudioSource> ().volume = tickVolume;
		seconds = this.transform.FindChild ("Second");
		minutes = this.transform.FindChild ("Minute");
		hours = this.transform.FindChild ("Hour");
		pauseController = GameObject.Find ("God");
		userCamera = GameObject.Find ("MainCamera");
		//Rotate clock hands to initial positions
		hours.transform.RotateAround(this.transform.position, this.transform.up, 
		                             (float)(hourVal + minuteVal * minHourFrac + secondVal * secHourFrac) * hoursToDegrees);
		minutes.transform.RotateAround(this.transform.position, this.transform.up, 
		                               (float)(minuteVal + secondVal * secMinFrac) * minutesToDegrees);
		seconds.transform.RotateAround(this.transform.position, this.transform.up, 
		                               (float)secondVal * secondsToDegrees);
		timeCount = 0;
	}

	private void run() {
		timeUpdate ();
		clockTickPosUpdate ();
		lookingAt ();
	}

	private void timeUpdate() {
		timeCount += Time.deltaTime;
		while (timeCount > 1) {
			tickFlag = true;
			timeCount -= 1;
			secondVal += 1;
			while(secondVal >= 60){
				secondVal -= 60;
				minuteVal += 1;
				while(minuteVal >= 60){
					minuteVal -= 60;
					hourVal += 1;
					while(hourVal >= 12){
						hourVal -= 12;
					}
				}
			}
		}
	}

	private void clockTickPosUpdate(){
		if (tickFlag) {
			//At least one second has passed since last clock position update
			tickFlag = false;
			seconds.RotateAround(this.transform.position, this.transform.up,
			                     (float)(secondsToDegrees));
			minutes.RotateAround(this.transform.position, this.transform.up,
				                 (float) (secMinFrac * minutesToDegrees));
			hours.RotateAround(this.transform.position, this.transform.up,
					             (float) (secHourFrac * hoursToDegrees));

			if(playTickingSound){
				//check if ticks are being delayed
				if (tickDelayCounter <= 0) {
					//if delay has finished, play the tick on each second update
					audio.PlayOneShot (tickingSound, 1.0f);
				}
				else {
					//if delay has not finished, decrememnt the delay counter
					tickDelayCounter--;
				}
			}
		}
	}

	private void lookingAt(){
		//Calculate vector from User Camera position to clock position
		Vector3 toThis = this.transform.position - userCamera.transform.position;
		//use Dot Product to calculate how closely the camera is pointed at the clock.
		//a dot product of -1 indicates that the user's camera is facing the opposite direction from the camera.
		//a dot procuct of 1 indicates that the user's camear is facing directly at the camera's origin point
		if ((Vector3.Dot (toThis.normalized, Camera.main.transform.forward) + 1) / 2.0 >= triggerAccuracy) {
			//Camera is facing the clock
			//Check if user has been looking at clock for at least 2 consecutive frames.
			//Duration can only be determined if the user is looking at the clock over 2 frames.
			if(lookingAtClock){
				//Record how long the user is looking at the clock.
				timeLookedAt += Time.deltaTime;
				//print total time looked at clock to console
				print (timeLookedAt);
			}
			lookingAtClock = true;
		} else {
			lookingAtClock = false;
		}
	}

	//called during each frame update
	void Update () {
		if (pauseController.GetComponent<GODPause> ().isRunning ()) {
			this.run();
		} else {

		}	
	}
}