Name Folders/Files by the convention StudentX, where X is a number.
The number to be used for the first student are as follows:
	Diana: 0
	Pei: 1
	Jonathan: 2
	Chris: 3

Each additial model you save should increase the number by 4.
This should result in a sequence following this pattern:
	Diana: 0, 4, 8, 12, 16, ...
	Pei: 1, 5, 9, 13, 17, ...
	Jonathan: 2, 6, 10, 14, 18, ...
	Chris: 3, 7, 11, 15, 19, ...

This process will only apply to the alpha models.